import { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid"; // Import the UUID function
import {
  isMobile,
  isTablet,
  browserName,
  osName,
  isIPad13,
} from "react-device-detect"; // Import the necessary functions from react-device-detect

interface DeviceInfo {
  userAgent: string;
  screenDimensions: string;
  deviceId: string;
  ipAddress: string | null;
  isIPad: boolean;
  isMobile: boolean;
  isTablet: boolean;
  browser: string;
  operatingSystem: string;
}

export const UserDeviceInfo = () => {
  const [deviceInfo, setDeviceInfo] = useState<DeviceInfo>({
    userAgent: navigator.userAgent,
    screenDimensions: `${window.screen.width}x${window.screen.height}`,
    deviceId: "",
    ipAddress: null,
    isIPad: isIPad13,
    isMobile: isMobile,
    isTablet: isTablet,
    browser: browserName,
    operatingSystem: osName,
  });

  useEffect(() => {
    const getDeviceId = () => {
      let deviceId = localStorage.getItem("device_id");
      if (!deviceId) {
        deviceId = uuidv4(); // Generate a UUID
        localStorage.setItem("device_id", deviceId);
        console.log(deviceId, " from userdeviceInfo");
      }
      return deviceId;
    };

    const fetchIpAddress = async () => {
      try {
        const response = await fetch("https://api.ipify.org?format=json");
        const data = await response.json();
        setDeviceInfo((prevState) => ({ ...prevState, ipAddress: data.ip }));
      } catch (error) {
        console.error("Failed to fetch IP address:", error);
        setDeviceInfo((prevState) => ({
          ...prevState,
          ipAddress: "Unavailable",
        }));
      }
    };

    setDeviceInfo((prevState) => ({ ...prevState, deviceId: getDeviceId() }));
    fetchIpAddress();
  }, []);

  return deviceInfo;
};
