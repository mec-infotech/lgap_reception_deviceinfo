import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet } from "react-native";
import { useEffect, useState } from "react";
import * as Font from "expo-font";
import { Login } from "./src/screens/login/Login";
import { signInWithEmailAndPassword, onAuthStateChanged } from "firebase/auth";
import { userAuth } from "./src/config/firebaseConfig";

const Stack = createNativeStackNavigator();
export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const [user, setUser] = useState<any | null>(null);
  const [initializing, setInitializing] = useState(true);

  useEffect(() => {
    const loadFonts = async () => {
      await Font.loadAsync({
        SpaceMono: require("./src/assets/fonts/SpaceMono-Regular.ttf"),
        HiraginoKaku_GothicPro_Text: require("./src/assets/fonts/Hiragino-Kaku-Gothic-Pro-W3.otf"),
        HiraginoKaku_GothicPro_Text_Bold: require("./src/assets/fonts/Hiragino-Kaku-Gothic-Pro-W6.ttf"),
      });

      setFontLoaded(true);
    };
    loadFonts();

    //FirebaseのsignInWithEmailAndPasswordメソッドを使用し、アプリ起動時に指定ユーザーで自動サインインを行う
    signInWithEmailAndPassword(
      userAuth,
      "city-staff-base@matsusaka.co.jp",
      "Pg37gRm20b58z3D5"
    ).catch((error) => {
      console.log("Auto sign-in failed:", error);
    });

    //ユーザーの認証状態の変更を監視する
    const unsubscribeAuthState = onAuthStateChanged(userAuth, (currentUser) => {
      setUser(currentUser);
      if (initializing) setInitializing(false);
    });

    // コンポーネントのアンマウント時にリスナーを解除
    return unsubscribeAuthState;
  }, []);

  if (!fontLoaded) {
    return null;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
          animation: "none",
        }}
      >
        <Stack.Screen name="Login" component={Login} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
